﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PowerPulse : MonoBehaviour {

    public Color colorPulseFrom;
    public Color colorPulseTo;
    public ProgressBar progressBar;

    private Image image;
    private Color c;

// Use this for initialization
	void Start () {
        image = GetComponent<Image>();
        colorPulseFrom = image.color;
	}
	
	// Update is called once per frame
	void Update () {
        //color = Color.Lerp(Color.green, Color.red, Time.time);
        if (progressBar.fraction >= 1)
        {
            GetComponent<Image>().color = Color.Lerp(colorPulseFrom, colorPulseTo, Mathf.PingPong(Time.time, 1.0f));
        }
        else
        {
            c.a = 0;
            image.color = c;
        }
          
    }
}
