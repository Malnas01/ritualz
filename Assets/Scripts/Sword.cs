﻿using UnityEngine;
using System.Collections;

public class Sword : RitualToken {

    Vector3 spawnPoint;
    Quaternion spawnRotation;
    PlayerScript hitPlayer;

    public Transform start, end;
    public float radius, extensionLength, extensionRadius;

    public Transform flyTarget;
    public float flySpeed;
    public PlayerScript owner;
    public bool active = false;
    public ProjectorPulse pulser;

    public float spinSpeed = 1f;
    bool spin = false;

    public AudioSource swordClank;
    public AudioSource summon;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(start.position, radius);
        Gizmos.DrawWireSphere(end.position, radius);
    }

    void Start()
    {
        spawnPoint = transform.position;
        spawnRotation = transform.rotation;
    }


    public PlayerScript CheckCollisions()
    {
        
        RaycastHit[] hits;
        hits = Physics.SphereCastAll(start.position, radius, end.position-start.position, Vector3.Distance(start.position, end.position), 1 << 9);
        foreach (RaycastHit hit in hits)
        {
            PlayerScript p = hit.transform.GetComponent<PlayerScript>();
            if (p != heldBy)
                return p;
        }
        return null;
    }

    public bool WillCollideWithPlayer(PlayerScript p)
    {
        RaycastHit[] hits;
        hits = Physics.SphereCastAll(start.position, radius + extensionRadius, end.position - start.position, Vector3.Distance(start.position, end.position) + extensionLength, 1 << 9);
        foreach (RaycastHit hit in hits)
        {
            PlayerScript hitPlayer = hit.transform.GetComponent<PlayerScript>();
            if (hitPlayer == p)
                return true;
        }
        return false;
    }


    void ResetPos()
    {
        transform.parent = null;
        transform.position = spawnPoint;
        transform.rotation = spawnRotation;
        flyTarget = null;
        active = false;
    }

    void Update()
    {
        if (flyTarget != null)
        {
            Vector3 target = flyTarget.position;
            target.y = transform.position.y;
            transform.position = Vector3.MoveTowards(transform.position, target, flySpeed);
            if (transform.position == target)
            {
                owner.hands.Pickup(this);
                flyTarget = null;
            }
        }

        if(spin)
        {
            transform.Rotate(new Vector3(spinSpeed, 0, 0));
        }
    }

    void FixedUpdate()
    {
        if (heldBy != null)
        {
            PlayerScript p = CheckCollisions();
            if (p != null)
            {
                if (p.hands.CarriedObject is Sword && ((Sword)p.hands.CarriedObject).WillCollideWithPlayer(heldBy))
                {
                    p.hands.Drop();
                    heldBy.hands.Drop();
                    if(swordClank != null)
                    swordClank.Play();
                }
                else {
                    p.Kill();
                    heldBy.hands.Drop();
                    ResetPos();
                }
            }
        }
    }

    public void Ready()
    {
        spin = true;
    }

    public void FlyAndEquip(PlayerScript player)
    {
        if (summon != null)
        {
            summon.time = 0.8f;
            summon.Play();
        }
        spin = false;
        active = true;
        flyTarget = player.transform;
        pulser.Pulse();
    }

    public void DisableAndReturn()
    {
        if (owner.hands.CarriedObject == this)
            owner.hands.Drop();
        ResetPos();
    }
}
