﻿using UnityEngine;
using System.Collections;

public class RitualToken : MonoBehaviour {

    public enum TokenType {
        Baby,
        Book,
        Candle,
        Eye,
        Duck,
        Heart,
        Jar,
        Skull,
        ToiletPaper,
        Vial,
        Invalid
    };
    public TokenType Type;
    public PlayerScript heldBy;
    public PlayerScript justDropped;
    public Pedestal pedestal;
}
