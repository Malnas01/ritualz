﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour {


    public Transform[] points;
    Transform _myTransform;
    Camera _myCamera;
    public float moveSpeed = 1f;
    public float buffer = 0.7f;
    public float minSize = 3;
    public bool frozen = true;

    
    void Start()
    {
        _myTransform = GetComponent<Transform>();
        _myCamera = GetComponent<Camera>();
    }
	
	// Update is called once per frame
	void Update () {
        if (!frozen)
        {
            Vector3 avg = new Vector3();
            float maxX = 0, maxZ= 0, minX = 0, minZ = 0;
            foreach (Transform t in points)
            {
                avg += t.position;
                maxX = Mathf.Max(t.position.x,maxX);
                minX = Mathf.Min(t.position.x, minX);
                maxZ = Mathf.Max(t.position.z, maxZ);
                minZ = Mathf.Min(t.position.z, minZ);
            }
            avg /= points.Length;
            avg.y = _myTransform.position.y;
            //middle.y = _myTransform.position.y;

            //Vector3 middle = Vector3.Lerp(p1.position, p2.position, 0.5f);
            //middle.y = _myTransform.position.y;
            //_myTransform.position = Vector3.MoveTowards(_myTransform.position, middle, moveSpeed);
            //float xDist = Mathf.Abs(p1.position.x - p2.position.x) * buffer;
            //float yDist = Mathf.Abs(p1.position.z - p2.position.z) * buffer;

            _myTransform.position = Vector3.MoveTowards(_myTransform.position, avg, moveSpeed);
            float xDist = Mathf.Max(maxX - _myTransform.position.x, _myTransform.position.x - minX) * buffer;
            float zDist = Mathf.Max(maxZ - _myTransform.position.z, _myTransform.position.z - minZ) * buffer;

            _myCamera.orthographicSize = Mathf.Max(xDist / _myCamera.aspect, zDist, minSize);
        }


    }
}
