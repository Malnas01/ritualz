﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Projector))]
public class ProjectorPulse : MonoBehaviour {


    public float size, speed;
    Projector projector;

	// Use this for initialization
	void Start () {
        projector = GetComponent<Projector>();
        gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	    if(gameObject.activeSelf)
        {
            projector.orthographicSize += speed * Time.deltaTime;
            if(projector.orthographicSize >= size)
            {
                projector.orthographicSize = 1;
                gameObject.SetActive(false);
            }
        }
	}

    public void Pulse()
    {
        projector.orthographicSize = 1;
        gameObject.SetActive(true);
    }



}
