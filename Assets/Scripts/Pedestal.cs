﻿using UnityEngine;
using System.Collections;

public class Pedestal : MonoBehaviour {

    public RitualToken requiredToken;
    public Transform mountPoint;
    public PentagramManager PentagramManager;
    public PlayerScript player;
    public LevelManager LevelManager; //  This value is set in PentagramManager
    public Transform filledParticleEffect; // Which effect to show when it's "Complete"
    private ParticleSystem particles;
    MeshRenderer _renderer;
    private bool _complete = false;
    public bool complete
    {
        get
        {
            return _complete;
        }

        set
        {
            _complete = value;
            _renderer.enabled = !_complete;
            particles.enableEmission = _complete;
        }
    }
    

    private PlayerScript playerWithToken;
    public Transform particleTarget;

    void Start()
    {
        _renderer = GetComponent<MeshRenderer>();
        particles = Instantiate(filledParticleEffect).GetComponent<ParticleSystem>();
        particles.transform.position = transform.position;
        Debug.Log(particles.transform.position.x + " " + particles.transform.position.y + ", " + particleTarget.position.x + " " + particleTarget.position.y);
        particles.transform.LookAt(particleTarget);
        particles.enableEmission = false;
    }

    void Update()
    {
    }


    public bool CanRemove()
    {
        if (LevelManager.winningPlayer == "")
        {
            return true;
        }
        else
            return false;
    }

    void OnTriggerEnter(Collider trigger)
    {
        if (trigger.tag == "Pickupable") // Check for Token tag. 
        {
            playerWithToken = trigger.GetComponent<RitualToken>().heldBy; // Will need this later to prevent from accidentally stacking the enemy pentagram.
           
            if (trigger.GetComponent<RitualToken>().Type == requiredToken.Type && !complete && player == playerWithToken)
            {
                // Object in player hands is the same token as the required for this slot. Mark this slot complete.
                complete = true;

                RitualToken token = playerWithToken.GetComponentInChildren<PickupPoint>().Drop();
                token.pedestal = this;
                token.transform.parent =  mountPoint.transform;
                token.transform.localPosition = Vector3.zero;

                if (LevelManager.winningPlayer == "") // Make sure that the game is still in progress and we do not have a winning player
                {
                    PentagramManager.CheckWinCondition(); 
                }
            
              
            }

        }
    }
}
