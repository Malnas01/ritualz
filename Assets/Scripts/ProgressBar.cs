﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(RectTransform))]
public class ProgressBar : MonoBehaviour
{

    [Range(0.0f, 1.0f)]
    public float fraction;
    public RectTransform bar;
    private RectTransform background;
    public bool vertical = false;

    // Use this for initialization
    void Start()
    {

        if (background == null)
            background = GetComponent<RectTransform>();

        int i = 0;
        while (bar == null && i < transform.childCount)
        {
            bar = transform.GetChild(i).GetComponent<RectTransform>();
            i++;
        }

        if (bar == null)
        {
            Debug.LogWarning("Health Bar has no appropriate child. Creating one...");
            GameObject g = new GameObject("Bar");
            bar = g.AddComponent<RectTransform>();
            g.transform.parent = background;
        }





    }

    // Update is called once per frame
    void OnGUI()
    {
        if (vertical)
        {
            float h = background.rect.height * fraction;
            bar.sizeDelta = new Vector2(bar.sizeDelta.x, h);
        }
        else
        {
            float w = background.rect.width * fraction;
            bar.sizeDelta = new Vector2(w, bar.sizeDelta.y);
        }
    }
}