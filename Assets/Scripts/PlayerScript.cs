﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

    public float movementSpeed = 1;
    public float speedModifier = 1;
    public string playerControlPrefix;
    private CharacterController playerController;

    public float power =0f;
    public float powerAccumulation = 0.1f;
    public ProgressBar ProgressBar;

    public Vector3 spawnPoint;

    public string playerName = "Default Player";

    public RitualToken nearbyItem;

    private Vector2 vel;
    private float power_timer = 0f;
    private bool gainPower = true;

    public PickupPoint hands;

    public bool frozen = true;

    public Sword sword;

    public AudioSource deathSound;
    

	// Use this for initialization
	void Start () {
        playerController = GetComponent<CharacterController>();
        hands = GetComponentInChildren<PickupPoint>();
        spawnPoint = transform.position;
        sword.owner = this;        
    }
	
	// Update is called once per frame
	void Update () {
        if (!frozen)
        {
            /*
            if (Input.GetKey(KeyCode.W))
            {
                vel.y += 1;
            }
            if (Input.GetKey(KeyCode.S))
            {
                vel.y -= 1;
            }
            if (Input.GetKey(KeyCode.A))
            {
                vel.x -= 1;
            }
            if (Input.GetKey(KeyCode.D))
            {
                vel.x += 1;
            }
            */

            Vector2 input = new Vector2(Input.GetAxis(playerControlPrefix + "Horizontal"), Input.GetAxis(playerControlPrefix + "Vertical"));
            input.Normalize();

            vel += input;
            vel /= 1.2f;
            playerController.Move(new Vector3(vel.x, 0, vel.y) * Time.deltaTime * movementSpeed * speedModifier);

            if (vel.sqrMagnitude > 0.1f) // Cutt off rotation if not moving.
            {
                transform.rotation = Quaternion.Euler(0, Mathf.Rad2Deg * Mathf.Atan2(vel.x, vel.y), 0);
            }
            
            if (Input.GetButtonDown(playerControlPrefix + "Fire1"))
            {
                if (hands.CarriedObject != null)
                    hands.Drop();

                //If there is an item nearby AND it is not being held AND it is not on a pedestal OR that pedestal is able to be removed from
                if (nearbyItem != null && nearbyItem.heldBy == null && (nearbyItem.pedestal == null || nearbyItem.pedestal.CanRemove()))
                {
                    Sword sword = nearbyItem as Sword;
                    if (sword == null || (sword.owner == this && sword.active))
                    {
                    hands.Pickup(nearbyItem);
                }
            }
            }

            if (Input.GetButtonDown(playerControlPrefix + "Fire2") && power >= 1)
            {
                UseAbility();
            }

            if (gainPower)
            IncreasePower();
            else
                DrainPower();
                
        
            
        }
    }

    public void Kill()
    {
        sword.DisableAndReturn();
        if (hands.CarriedObject != null) {
            hands.Drop().justDropped = null;
        }
        if (!gainPower) {
            power_timer = 0;
            gainPower = true;
        }
        if (deathSound!= null)
            deathSound.Play();
        transform.position = spawnPoint;
    }

    public void UseAbility()
    {
        ProgressBar.fraction = power; //Update HUD
        gainPower = false; // Drain power because we're using an ability

        sword.FlyAndEquip(this);
    }


    public RitualToken.TokenType getObjectInHands()
    {
        return hands.GetTokenType();
    }

    public void IncreasePower()
    {
        if (power_timer < 10)
        {
            power_timer += Time.deltaTime;
            power = power_timer / 10;
            ProgressBar.fraction = power;
        } else
        {
            power_timer = 10;
            sword.Ready();
        }

    }

    public void DrainPower()
    {
        power_timer -= Time.deltaTime;
        power = power_timer / 10;
        ProgressBar.fraction = power;

        if (power <= 0)
        {
            gainPower = true;
            sword.DisableAndReturn();
        }
            
    }

}
