﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public PentagramManager[] pentagrams;
    public List<Transform> tokenSpawnPoints;
    
    public float startTimer = 3.0f;
    bool started = false;
    public CameraController cameraController;
    public PlayerScript[] players;
    public Text countdownText;
    public Sword sword;
    public string winningPlayer;
    public Text victoryText;
    public float resetLevelTimer = 5.0f;
    public Scene workingLevels;
    public Transform directionalLight;

    public static Scene[] Scenes;

    public bool debug = true; // May use for debugging, leave the variable defined even if no longer used in this script
    
    

	// Use this for initialization
	void Start () {
        // Change daylight
        directionalLight.rotation = Quaternion.Euler(Random.Range(40, 140), Random.Range(80, 100), 0);

        for (int i = 0; i < pentagrams.Length; i++)
        {
            pentagrams[i].LevelManager = this;
            pentagrams[i].SpawnTriggers();
            

            for(int j = 0; j < pentagrams[i].pedestalPositions.Length; j++)
            {

                int k = Random.Range(0, tokenSpawnPoints.Count);
                RitualToken t = (Instantiate(pentagrams[i].pedestalPositions[j].requiredToken, tokenSpawnPoints[k].position, tokenSpawnPoints[k].rotation) as RitualToken);
                //transform.parent = map.transform;
                tokenSpawnPoints.RemoveAt(k);
            }
        }  
    }
	
	// Update is called once per frame
	void Update () {
        if (!started)
        {
            startTimer -= Time.deltaTime;
            countdownText.text = ((int)startTimer + 1).ToString();
            countdownText.fontSize = (int) (300 * (startTimer % 1));
            if(startTimer <= 0)
            {
                countdownText.gameObject.SetActive(false);
                started = true;
                cameraController.frozen = false;
                foreach (PlayerScript p in players)
                    p.frozen = false;
            }
        }
    }

    public void PlayerWon(PlayerScript player)
    {
        // Implement Win logic
        
        if (winningPlayer == "")
        {           
            winningPlayer = player.playerName;
            victoryText.text = winningPlayer + " wins this round!";
            StartCoroutine(ReloadLevel());
        }

        foreach(PentagramManager p in pentagrams)
        {
            p.UncompletePedestals();
        }

    }


    IEnumerator ReloadLevel()
    {
        yield return new WaitForSeconds(resetLevelTimer); // Wait for Level Timer

        int numberOfScenes = SceneManager.sceneCountInBuildSettings;
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;


        // Reload the Map if we only have one scene in the Build Settings.
        if (numberOfScenes == 1)
        {
            SceneManager.LoadScene(currentSceneIndex);
        }        
        else // Cycle through remaining scenes in a sequential order.
        {
            int nextSceneIndex = (currentSceneIndex + 1) % numberOfScenes;
            SceneManager.LoadScene(nextSceneIndex); // Next scene ---->>>> currentSceneIndex + 1 % numberOfScenes
        }


    }
}
