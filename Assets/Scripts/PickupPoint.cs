﻿using UnityEngine;
using System.Collections;

public class PickupPoint : MonoBehaviour
{
    private PlayerScript player;
    private BoxCollider boxCollider;
    public RitualToken CarriedObject
    {
        get; private set;
    }

    // Use this for initialization
    void Start()
    {
        player = GetComponentInParent<PlayerScript>();
        
        boxCollider = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public RitualToken.TokenType GetTokenType()
    {
        if (CarriedObject == null)
        {
            return RitualToken.TokenType.Jar;
        }
        return CarriedObject.Type;
    }

    public void Pickup(RitualToken token)
    {
        if (CarriedObject != null)
            Drop();
        CarriedObject = token;
        CarriedObject.transform.parent = transform;
        CarriedObject.transform.localPosition = Vector3.zero;
        CarriedObject.transform.localRotation = Quaternion.identity;
        CarriedObject.heldBy = player;
        if(CarriedObject.pedestal != null)
            CarriedObject.pedestal.complete = false;

        if(!(token is Sword))
            player.speedModifier = 0.7f;
    }

    public RitualToken Drop()
    {
        player.speedModifier = 1f;
        if (CarriedObject != null)
        {
            RitualToken droppedObject = CarriedObject;
            CarriedObject.transform.parent = null;
            // Drop the carriedObject
            CarriedObject.heldBy = null;
            CarriedObject.justDropped = player;
            CarriedObject = null;
            return droppedObject;
        }
        else
            return null;
        
    }

    void OnTriggerEnter(Collider collisionInfo)
    {
        if(collisionInfo.tag.Equals("Pickupable"))
        {
            RitualToken item = collisionInfo.GetComponent<RitualToken>();
            if (item.heldBy == null && item.justDropped == player)
                item.justDropped = null;
            else
                player.nearbyItem = item;

        }

    }

    void OnTriggerExit(Collider collisionInfo)
    {
        if (collisionInfo.tag.Equals("Pickupable") && player.nearbyItem == collisionInfo.GetComponent<RitualToken>())
        {
            player.nearbyItem = null;
        }
    }
}
