﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;

public class PentagramManager : MonoBehaviour {

    public Pedestal[] pedestalPositions;
    public List<Pedestal> allAvailablePedestals;
    public PlayerScript player;
    public LevelManager LevelManager;
    public Transform pizza;
    public ParticleSystem particle;
    public AudioSource pizplosion;


    public void SpawnTriggers()
    {

        for (int i = 0; i < pedestalPositions.Length; i++)
        {
            int theOneWeAreSpawning = Random.Range(0, allAvailablePedestals.Count);
            Pedestal t = Instantiate(allAvailablePedestals[theOneWeAreSpawning], pedestalPositions[i].transform.position, pedestalPositions[i].transform.rotation) as Pedestal;
            t.filledParticleEffect = pedestalPositions[i].filledParticleEffect;
            t.particleTarget = pedestalPositions[i].particleTarget;
            Destroy(pedestalPositions[i].gameObject);
            pedestalPositions[i] = t;
            t.PentagramManager = this;
            t.LevelManager = LevelManager;
            t.player = player;
            t.transform.parent = transform;
            allAvailablePedestals.RemoveAt(theOneWeAreSpawning);
        }
    }

    public void SpawnPizza()
    {
        particle.Play();
        if (pizplosion != null)
            pizplosion.Play();
        for (int i = 0; i < 8; i++)
        {
            float directionDegrees = ((float)i * 360.0f / 8.0f);
            float distance = -2.0f; // Distance from centre, negative as the pizza needs to face the opposite direction as it needs to move in. (So that it faces centre)
            Vector3 vec = new Vector3(Mathf.Sin(Mathf.Deg2Rad * directionDegrees), -0.1f, Mathf.Cos(Mathf.Deg2Rad * directionDegrees)) * distance;
            Transform slice = Instantiate(pizza) as Transform;
            slice.parent = transform;
            slice.localPosition = vec;
            slice.localRotation = Quaternion.Euler(0, directionDegrees, 0);
        }
    }
	
    public void UncompletePedestals()
    {
        foreach (Pedestal pedestal in pedestalPositions)
        {
            pedestal.complete = false;
        }
    }

	public void CheckWinCondition ()
    {
        foreach(Pedestal pedestal in pedestalPositions)
        {
            if (pedestal.complete == false) { 
                return; // Return when we find first incomplete pedestal. No point in going further in the list.
            }

            
        }

        

        LevelManager.PlayerWon(player); // Tell level manager that we have a winner. This is only executed if all pedestals are complete
        SpawnPizza();

    }


}
