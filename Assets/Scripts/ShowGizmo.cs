﻿using UnityEngine;
using System.Collections;

public class ShowGizmo : MonoBehaviour {

    public Color color = Color.white;
    public float radius = 0.5f;

    void OnDrawGizmos()
    {
        Gizmos.color = color;
        Gizmos.DrawWireSphere(transform.position, radius);
    }

}
